import React from 'react';

class FormNouveauReleve extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            onClick: props.onClick
        };
    }

    componentDidMount() {
        fetch("http://localhost/react-02-api-php/villes.php")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items,
                        error: null,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render () {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <form>
                    <div className="form-group row">
                        <label for="inputVille" class="col-sm-2 col-form-label">Ville</label>
                        <div className="col-sm-10">
                            <select className="form-control" id="inputVille">
                                {this.state.items.map(item => (
                                    <option value={item.id} key={item.id}>{item.nom}</option>
                                ))}
                            </select>
                        </div>
                    </div>
                    <button type="button" name="btnEnregistrer" onClick={this.state.onClick}>
                        Enregistrer
                    </button>
                </form>
            );
        }
    }
}

export default FormNouveauReleve;