import React from 'react';
import FormNouveauReleve from './FormNouveauReleve';

class Releve extends React.Component {
    constructor(props) {
        super(props);
        this.state = {display: 'button'};
    }

    btnNouveauReleve_Click() {
        this.setState({display: 'form'});
    }

    btnEnregistrer_Click() {
        this.setState({display: 'button'});
    }

    render () {
        let render;
        switch(this.state.display)
        {
            case 'button':
                render = (
                    <button name="btnNouveauReleve" onClick={() => this.btnNouveauReleve_Click()}>
                        Nouveau Relevé
                    </button>
                );
                break;

            case 'form':
                render = (
                    <FormNouveauReleve onClick={() => this.btnEnregistrer_Click()} />
                );
                break;
            default:
                break;
        }

        return render;
    }
}

export default Releve;