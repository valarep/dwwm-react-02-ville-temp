import React from 'react';
import './App.css';
import Navbar from './Navbar';
import ListeVille from './ListeVille';

function App() {
  return (
    <div className="App">
      <header>
        <Navbar />
      </header>
      <main className="container">
        <ListeVille />  
      </main>
    </div>
  );
}

export default App;
