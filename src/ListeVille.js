import React from 'react';
import './ListeVille.css';
import Releve from './Releve';

class ListeVille extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
        };
    }

    componentDidMount() {
        this.tick();
        this.timer = setInterval(() => this.tick(), 2000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick()
    {
        fetch("http://localhost/react-02-api-php/temperatures.php")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result.items,
                        error: null,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }

    render()
    {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <table id="ListeVille" className="col-xs-auto col-md-6">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Température</th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map(item => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td>{item.nom}</td>
                                <td>{item.temperature}</td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colSpan="3">
                                <Releve />
                            </th>
                        </tr>
                    </tfoot>
                </table>
            );
        }
    }
}

export default ListeVille;